//
//  CurrencyExchangeMockServices.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//Copyright © 2019 Destplay. All rights reserved.
//

import Foundation

class CurrencyExchangeMockServices: CurrencyExchangeServicesDataSource {
    let mockManager = CurrencyExchangeMockManager()
    var jsonMockType = CurrencyExchangeJsonMockType.success
    
    init(jsonMockType: CurrencyExchangeJsonMockType) {
        self.jsonMockType = jsonMockType
    }
    
    func fetch(objectFor interactor: CurrencyExchangeInteractorDelegate?) {
        guard let data = self.mockManager.fileJson(fileName: self.jsonMockType.getNameFile()) else {
            let error = NSError(domain: "Данные не найдены", code: 0)
            interactor?.responseCurrences(error)
            return
        }
        
        self.mockManager.parseJson(data: data, success: { (object: CurrencyExchangeModelResponse) in
            
            interactor?.responseCurrences(object)
            
        }, failure: { error in
            interactor?.responseCurrences(error as NSError)
        })
    }
    
    deinit {
        Log.e("deinit mock services", tag: "DEINIT")
    }
}

// Поскольку модуль всего один, лежит здесь
class CurrencyExchangeMockManager {
    func fileJson(fileName: String) -> Data? {
        do {
            guard let file = Bundle.main.url(forResource: fileName, withExtension: "json") else {
                return nil
            }
            let data = try Data(contentsOf: file)
            
            Log.i(String(data: data, encoding: .utf8), tag: String(describing: "RESPONSE"))
            
            return data
        } catch {
            
            return nil
        }
    }
    
    func parseJson<T: Decodable>(data: Data, success: ((T) -> ()), failure: ((Error) -> ())) {
        do {
            let model = try JSONDecoder().decode(T.self, from: data)
            success(model)
        } catch {
            failure(error as NSError)
        }
    }
}
