//
//  CurrencyExchangeNetworkServices.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//Copyright © 2019 Destplay. All rights reserved.
//

import UIKit

class CurrencyExchangeNetworkServices: CurrencyExchangeServicesDataSource {
    let networkManager = CurrencyExchangeNetworkManager()
    
    func fetch(objectFor interactor: CurrencyExchangeInteractorDelegate?) {
        self.networkManager.request(success: { (data) in
            
            self.networkManager.parseJson(data: data, success: { (model: CurrencyExchangeModelResponse) in
               interactor?.responseCurrences(model)
            }, failure: { error in
                interactor?.responseCurrences(error as NSError)
            })
        }, failure: { error in
            interactor?.responseCurrences(error as NSError)
        })
    }
    
    deinit {
        Log.e("deinit network services", tag: "DEINIT")
    }
}

// Поскольку модуль всего один, лежит здесь
class CurrencyExchangeNetworkManager {
    func request(success: @escaping ((Data) -> ()), failure: @escaping ((Error) -> ())) {
        let url = URL(string: "https://api.exchangeratesapi.io/latest")!
        let request = NSMutableURLRequest(url: url)
        let session = URLSession.shared
        
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest) {
            data, response, error in
            
            guard let data = data else { failure(NSError(domain: "Ошибка данных", code: 0)); return }
            if let error = error {
                failure(error)
            }
            
            success(data)
            
            Log.i(String(data: data, encoding: .utf8), tag: String(describing: "RESPONSE"))
        }
        
        task.resume()
    }
    
    func parseJson<T: Decodable>(data: Data, success: ((T) -> ()), failure: ((Error) -> ())) {
        do {
            let model = try JSONDecoder().decode(T.self, from: data)
            success(model)
        } catch {
            failure(error as NSError)
        }
    }
}
