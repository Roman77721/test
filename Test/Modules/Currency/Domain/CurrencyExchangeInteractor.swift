//
//  CurrencyExchangeInteractor.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//Copyright © 2019 Destplay. All rights reserved.
//

import Foundation

class CurrencyExchangeInteractor {
    
    private weak var delegate: CurrencyExchangePresenterDelegate?
    private var dataSource: CurrencyExchangeServicesDataSource?
    private var model: CurrencyExchangeModel?
    
    init(dataSource repository: CurrencyExchangeServicesDataSource) {
        self.dataSource = repository
        
        Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.didFetchUpdate), userInfo: nil, repeats: true)
    }
    
    private func mappingModels(_ modelResponse: CurrencyExchangeModelResponse) -> Bool{
        if let model = self.model {
            return model.updateCurrency(rates: modelResponse.rates)
        } else {
            self.model = CurrencyExchangeModel(modelResponse)
            return true
        }
        
    }
    
    @objc func didFetchUpdate() {
        DispatchQueue.global(qos: .background).async {
            self.dataSource?.fetch(objectFor: self)
        }
    }
    
    deinit {
        Log.e("deinit interactor", tag: "DEINIT")
    }
}


extension CurrencyExchangeInteractor: CurrencyExchangeInteractorDelegate {
    func responseCurrences(_ model: CurrencyExchangeModelResponse) {
        guard self.mappingModels(model) else { return }
        if let model = self.model {
            DispatchQueue.global(qos: .userInteractive).async {
                 self.delegate?.responseCurrences(model)
            }
        }
    }
    
    func responseCurrences(_ error: NSError) {
        DispatchQueue.global(qos: .userInteractive).async {
            self.delegate?.responseCurrences(error)
        }
    }
}

extension CurrencyExchangeInteractor: CurrencyExchangeInteractorDataSource {
    func fetch(objectFor presenter: CurrencyExchangePresenterDelegate?) {
        self.delegate = presenter
        
        self.dataSource?.fetch(objectFor: self)
    }
    
    func сalculateCurrency(amount: Double, fromRate argA: Double, toRate argB: Double) -> Double {
        
        return (amount / argA * argB).roundAndLimitted()
    }
    
    func exchange(writeOff: Double, replenishment: Double, topName: String, bottomName: String) {
        if let model = self.model {
            model.updateCurrency(purse: -writeOff, name: topName)
            model.updateCurrency(purse: replenishment, name: bottomName)
            self.delegate?.responseCurrences(self.model!)
        }
    }
}
