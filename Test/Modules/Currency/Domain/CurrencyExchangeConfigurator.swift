//
//  CurrencyExchangeConfigurator.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//Copyright © 2019 Destplay. All rights reserved.
//

import UIKit

class CurrencyExchangeConfigurator {
    
    private var presenter: CurrencyExchangePresenterDataSource!
    
    init() {
        let dataServices = CurrencyExchangeNetworkServices()
        let interactor = CurrencyExchangeInteractor(dataSource: dataServices)
        self.presenter = CurrencyExchangePresenter(dataSource: interactor)
    }
    
    func getDataSource() -> CurrencyExchangePresenterDataSource {
        
        return self.presenter
    }
}
