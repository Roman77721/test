//
//  CurrencyExchangeEntities.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//Copyright © 2019 Destplay. All rights reserved.
//

import UIKit

import Foundation

/// Протокол делегат для взаимодействия с компонентом View
protocol CurrencyExchangeViewControllerDelegate: class {
    func responseCurrences()
    
    func showAlert(title: String, message: String?, handler: ((UIAlertAction) -> ())?)
}

/// Протокол для запроса ресурсов у Presenter'а
protocol CurrencyExchangePresenterDataSource: class {
    /// Метод для запроса информации
    func fetch(objectFor view: CurrencyExchangeViewControllerDelegate)
    
    /// Метод для получения списка с контентом для ячеек
    func getListCurrency(_ tag: CollectionTag) -> [CurrencyExchangeModelCell]
    
    /// Метод выполняется при смене ячейки
    func setItem(index: Int, tag: CollectionTag)
    
    /// Метод запрашивает актуальные ставки
    func getCalculate(_ value: String, tag: CollectionTag) -> Amounts
    
    /// Размен валюты
    func exchange()
}

/// Протокол делегат для взаимодействия с компонентом Presenter
protocol CurrencyExchangePresenterDelegate: class {
    /// Метод для передачи информации
    ///
    /// - Parameter model: модель данных
    func responseCurrences(_ model: CurrencyExchangeModel)
    
    /// Метод для передчи ошибки
    func responseCurrences(_ error: NSError)
}

/// Протокол для запроса ресурсов у Interactor'а
protocol CurrencyExchangeInteractorDataSource: class {
    /// Метод для запроса информации
    ///
    /// - Parameter presenter: текущий делегат
    func fetch(objectFor presenter: CurrencyExchangePresenterDelegate?)
    
    /// Данный метод расчитывает курс валюты
    func сalculateCurrency(amount: Double, fromRate: Double, toRate: Double) -> Double
    
    /// Метод для обмена валют
    func exchange(writeOff: Double, replenishment: Double, topName: String, bottomName: String)
}

/// Протокол делегат для взаимодействия с компонентом Intractor
protocol CurrencyExchangeInteractorDelegate: class {
    /// Метод для передачи информации
    ///
    /// - Parameter text: текст
    func responseCurrences(_ model: CurrencyExchangeModelResponse)
    
    /// Метод для передчи ошибки
    ///
    /// - Parameter error: ошибка
    func responseCurrences(_ error: NSError)
}

/// Протокол для запроса ресурсов у Repository
protocol CurrencyExchangeRepositoryDataSource: class {
    /// Метод для запроса информации
    ///
    /// - Parameter presenter:  текущий делегат
    func fetch(objectFor interactor: CurrencyExchangeInteractorDelegate?)
}

/// Протокол для запроса ресурсов у сервисов
protocol CurrencyExchangeServicesDataSource: class {
    /// Метод для запроса информации
    ///
    /// - Parameter presenter:  текущий делегат
    func fetch(objectFor interactor: CurrencyExchangeInteractorDelegate?)
}

protocol CurrencyExchangeCellDelegate: class {
    
    /// Метод для обновления суммы
    func setAmount(value: String, tag: CollectionTag)
}

/// Перечисление моковых json
/// - success: Положительный ответ
/// - empty: Ссписок пуст
enum CurrencyExchangeJsonMockType {
    case success, empty, error
    
    /// Метод для получения названия json файла
    ///
    /// - Returns: Название ввиде текста
    func getNameFile() -> String {
        switch(self) {
            case .success: return "CurrencyExchangeSuccess"
            case .empty: return "CurrencyExchangeEmpty"
            case .error: return "CurrencyExchangeError"
        }
    }
}

// Перечесление для определение нужного списка
enum CollectionTag: Int {
    case top = 0
    case bottom = 1
}

/// Структура основной модели обмена
struct CurrencyExchangeModelResponse: Decodable {
    var base: String?
    var rates: Rates?
    var date: String?
    
    struct Rates: Decodable {
        var USD: Double?
        var GBP: Double?
    }
}

/// Основная модель  с валютами
class CurrencyExchangeModel {
    private var currences: [Currency]
    private var date: String
    private let queue = DispatchQueue(label: "ru.destplay.modelQueue", attributes: .concurrent)
    
    init(_ model: CurrencyExchangeModelResponse) {
        self.currences = [Currency]()
        self.currences.append(Currency(name: model.base ?? "EUR", rate: 1.00))
        self.date = model.date ?? ""
        if let rates = model.rates {
            self.currences.append(contentsOf: self.variablesToArray(rates: rates))
        }
    }
    
    /// Метод для обновления ставок по валютам
    /// 
    /// Return: если есть обновления возвращает положительный ответ
    func updateCurrency(rates: CurrencyExchangeModelResponse.Rates?) -> Bool {
        var update = Bool()
        let semaphor = DispatchSemaphore(value: 0)
        self.queue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            if let rates = rates {
                self.currences = self.currences.compactMap { item -> Currency in
                    var item = item
                    let newRate = self.variablesToArray(rates: rates).filter { $0.name == item.name }.first?.rate ?? item.rate
                    guard !item.rate.isEqual(to: newRate) else { semaphor.signal(); return item }
                    item.rate = self.variablesToArray(rates: rates).filter { $0.name == item.name }.first?.rate ?? item.rate
                    update = true
                    semaphor.signal()
                    
                    return item
                }
            }
        }
        semaphor.wait()
        return update
    }
    
    /// Метод для обновления кошелька валюты
    func updateCurrency(purse: Double, name: String) {
        self.queue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            if let index = self.currences.firstIndex(where: { $0.name == name }) {
                self.currences[index].purse = self.currences[index].purse + purse
            }
        }
    }
    
    /// Метод для получения списка валют
    func getCurrency() -> [Currency] {
        self.queue.sync {
            return self.currences
        }
    }
    
    /// Метод для для получения массива из параметров объекта
    private func variablesToArray(rates: CurrencyExchangeModelResponse.Rates) -> [Currency] {
        let mirror = Mirror(reflecting: rates)
        let array = mirror.children.compactMap { (item: Mirror.Child) -> Currency? in
            guard let name = item.label, let rate = item.value as? Double else { return nil }
            
            return Currency(name: name, rate: rate.roundAndLimitted())
        }
        
        return array
    }

}

/// Модель для отображения ячейки
struct CurrencyExchangeModelCell {
    var name: String
    var amount: String
    var purse: String
    var rate: String
    
    init(name: String, amount: String, purse: String, rate: String) {
        self.name = name
        self.amount = amount
        self.purse = purse
        self.rate = rate
    }
}

/// Структура для валюты
struct Currency {
    var name: String
    var rate: Double
    var purse: Double
    
    init() {
        self.name = "EUR"
        self.rate = 1.00
        self.purse = 100
    }
    
    init(name: String, rate: Double) {
        self.name = name
        self.rate = rate
        self.purse = 100
    }
}

/// Структура для возвращения актуальных сумм
struct Amounts {
    var fromAmount: String
    var toAmount: String
    var fromIndex: Int
    var toIndex: Int
    
    init(_ topAmount: String, _ bottomAmount: String, _ topIndex: Int, _ bottomIndex: Int) {
        self.fromAmount = topAmount
        self.toAmount = bottomAmount
        self.fromIndex = topIndex
        self.toIndex = bottomIndex
    }
}
