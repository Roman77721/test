//
//  Cell.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//  Copyright © 2019 Destplay. All rights reserved.
//

import UIKit

class CurrencyExchangeCell: UICollectionViewCell {
    
    @IBOutlet private weak var view: UIView!
    @IBOutlet weak var nameCurrencyLabel: UILabel!
    @IBOutlet private weak var amountTextField: UITextField!
    @IBOutlet private weak var purceLabel: UILabel!
    @IBOutlet private weak var rateLabel: UILabel!
    
    private weak var delegate: CurrencyExchangeCellDelegate!
    private var collectionTag: CollectionTag!
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func configuration(name: String, amount: String, purse: String, rate: String, delegate: CurrencyExchangeCellDelegate, tag: CollectionTag) {
        self.delegate = delegate
        self.collectionTag = tag
        self.amountTextField.addTarget(self, action: #selector(self.didTextEditing(_:)), for: .editingChanged)
        
        self.nameCurrencyLabel.text = name
        self.amountTextField.text = amount
        self.purceLabel.text = "You have: \(purse)"
        self.rateLabel.text = rate
        self.view.layer.cornerRadius = self.view.frame.height / 12
        self.view.backgroundColor = .init(white: 0.9, alpha: 0.7)
    }
    
    func set(amount: String) {
        DispatchQueue.main.async {
            self.amountTextField.text = amount
        }
    }
    
    @objc private func didTextEditing(_ textField: UITextField) {
        self.delegate.setAmount(value: textField.text ?? "", tag: self.collectionTag)
    }
}
