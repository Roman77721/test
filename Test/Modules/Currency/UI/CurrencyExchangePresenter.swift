//
//  CurrencyExchangePresenter.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//Copyright © 2019 Destplay. All rights reserved.
//

import Foundation

class CurrencyExchangePresenter {
    
    private weak var delegate: CurrencyExchangeViewControllerDelegate?
    private var dataSource: CurrencyExchangeInteractorDataSource!
    private var fromIndex = Int()
    private var toIndex = Int()
    private var fromItems = [CurrencyExchangeModelCell]()
    private var toItems = [CurrencyExchangeModelCell]()
    private var currences = [Currency]()
    
    init(dataSource interactor: CurrencyExchangeInteractorDataSource?) {
        self.dataSource = interactor
    }
    
    // Метод для подготовки соотношения курсов валют, к показу
    private func getRates(_ tag: CollectionTag) -> String {
        let amount = 1.00
        var result = Double()
        
        var firstCurrencyChar = String()
        var lastCurrencyChar = String()
        
        switch tag {
            case .top:
                result = dataSource.сalculateCurrency(amount: amount, fromRate: self.currences[self.fromIndex].rate, toRate: self.currences[self.toIndex].rate)
                firstCurrencyChar = Formating().getSymbol(forCurrencyCode: self.currences[self.fromIndex].name)
                lastCurrencyChar = Formating().getSymbol(forCurrencyCode: self.currences[self.toIndex].name)
            case .bottom:
                result = dataSource.сalculateCurrency(amount: amount, fromRate: self.currences[self.toIndex].rate, toRate: self.currences[self.fromIndex].rate)
                firstCurrencyChar = Formating().getSymbol(forCurrencyCode: self.currences[self.toIndex].name)
                lastCurrencyChar = Formating().getSymbol(forCurrencyCode: self.currences[self.fromIndex].name)
        }
        
        return "\(firstCurrencyChar)\(amount) = \(lastCurrencyChar)\(result)"
    }
    
    /// Метод для обновления объектов со списками
    private func updateAllItems() {
        self.fromItems = self.currences.compactMap { item -> CurrencyExchangeModelCell in
            let symbol = Formating().getSymbol(forCurrencyCode: item.name)
            let purse = "\(item.purse.roundAndLimitted())\(symbol)"
            return CurrencyExchangeModelCell(name: item.name, amount: "", purse: purse, rate: "")
        }
    
        self.toItems = self.currences.compactMap { item -> CurrencyExchangeModelCell in
            let symbol = Formating().getSymbol(forCurrencyCode: item.name)
            let purse = "\(item.purse.roundAndLimitted())\(symbol)"
            return CurrencyExchangeModelCell(name: item.name, amount: "", purse: purse, rate: "")
        }
    }
    
    deinit {
        Log.i("deinit presenter", tag: "DEINIT")
    }
}

extension CurrencyExchangePresenter: CurrencyExchangePresenterDataSource {
    func fetch(objectFor view: CurrencyExchangeViewControllerDelegate) {
        self.delegate = view
        self.dataSource?.fetch(objectFor: self)
    }
    
    func getListCurrency(_ tag: CollectionTag) -> [CurrencyExchangeModelCell] {
        self.fromItems[self.fromIndex].rate = self.getRates(tag)
        self.toItems[self.toIndex].rate = self.getRates(tag)
        switch(tag) {
            case .top:
                return self.fromItems
            case .bottom:
                return self.toItems
        }
    }

    func setItem(index: Int, tag: CollectionTag) {
        
        self.fromItems[self.fromIndex].amount = ""
        self.toItems[self.toIndex].amount = ""
        
        switch(tag) {
            case .top:
                guard self.fromIndex != index else { return }
                self.fromIndex = index
                self.delegate?.responseCurrences()
            case .bottom:
                guard self.toIndex != index else { return }
                self.toIndex = index
                self.delegate?.responseCurrences()
        }
    }
    
    func getCalculate(_ value: String, tag: CollectionTag) -> Amounts {
        guard let dataSource = self.dataSource, let amount = Double(value) else { return Amounts("", "", self.fromIndex, self.toIndex) }
        
        let topRate = self.currences[self.fromIndex].rate
        let bottomRate = self.currences[self.toIndex].rate
        var newValue = Double()
        var topAmount = String()
        var bottomAmount = String()
        
        switch(tag) {
            case .top:
                newValue = dataSource.сalculateCurrency(amount: amount, fromRate: topRate, toRate: bottomRate)
                topAmount = Formating().formatMask(text: value, typeMask: .negative)
                bottomAmount = Formating().formatMask(text: String(newValue), typeMask: .positive)
            case .bottom:
                newValue = dataSource.сalculateCurrency(amount: amount, fromRate: bottomRate, toRate: topRate)
                topAmount = Formating().formatMask(text: String(newValue), typeMask: .negative)
                bottomAmount = Formating().formatMask(text: String(value), typeMask: .positive)
        }
        self.fromItems[self.fromIndex].amount = topAmount
        self.toItems[self.toIndex].amount = bottomAmount
        
        let amounts = Amounts(topAmount, bottomAmount, self.fromIndex, self.toIndex)
        
        return amounts
    }
    
    func exchange() {
        let topRate = self.currences[self.fromIndex].rate
        let bottomRate = self.currences[self.toIndex].rate
        let writeOff = Double(self.fromItems[self.fromIndex].amount)?.roundAndLimitted() ?? 0.00
        let replenishment = dataSource.сalculateCurrency(amount: writeOff, fromRate: topRate, toRate: bottomRate).roundAndLimitted()
        let topName = self.currences[self.fromIndex].name
        let bottomName = self.currences[self.toIndex].name
        let purse = self.currences[self.fromIndex].purse
        let symbol = Formating().getSymbol(forCurrencyCode: bottomName)
        
        if writeOff > purse {
            self.delegate?.showAlert(title: "Insufficient funts for replenishment", message: nil, handler: nil) 
        }
        
        if writeOff <= 0.00 {
            self.delegate?.showAlert(title: "Enter replenishment amount", message: nil, handler: nil)
        }
        
        var message = "Receipt \(symbol)\(replenishment) to account \(bottomName)."
        message.append("\nAvailable balance: \(symbol)\(purse)")
        message.append("\n")
        message.append("\n Available accounts:")
        for item in self.currences {
            let symbol = Formating().getSymbol(forCurrencyCode: item.name)
            message.append("\n\(symbol)\(item.purse)")
        }
        
        self.delegate?.showAlert(title: "Notification", message: message) { _ in
            self.dataSource?.exchange(writeOff: writeOff, replenishment: replenishment, topName: topName, bottomName: bottomName)
        }
    }
}

extension CurrencyExchangePresenter: CurrencyExchangePresenterDelegate {
    func responseCurrences(_ model: CurrencyExchangeModel) {
        self.currences = model.getCurrency()
        self.updateAllItems()
        self.delegate?.responseCurrences()
    }
    
    func responseCurrences(_ error: NSError) {
        self.delegate?.showAlert(title: error.domain, message: nil, handler: nil)
    }
}

