//
//  ViewController.swift
//  Test
//
//  Created by Макарский Р.Д. on 07/11/2019.
//  Copyright © 2019 Destplay. All rights reserved.
//

import UIKit

class CurrencyExchangeViewController: UIViewController {

    @IBOutlet weak var fromCollectionView: UICollectionView!
    @IBOutlet weak var toCollectionView: UICollectionView!
    
    private var dataSource: CurrencyExchangePresenterDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = CurrencyExchangeConfigurator().getDataSource()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            self.dataSource?.fetch(objectFor: self)
        }
        self.configurationCV(collectionView: self.fromCollectionView)
        self.configurationCV(collectionView: self.toCollectionView)
    }
    
    /// Конфигурация collectionView
    private func configurationCV(collectionView: UICollectionView) {
        DispatchQueue.main.async {
            let collectionViewLayout = UICollectionViewFlowLayout()
            collectionViewLayout.minimumLineSpacing = 0
            collectionViewLayout.scrollDirection = .horizontal
            collectionView.collectionViewLayout = collectionViewLayout
            collectionView.backgroundColor = .init(white: 1.0, alpha: 0.0)
            collectionView.register(UINib(nibName: "CurrencyExchangeCell", bundle: nil), forCellWithReuseIdentifier: "cvCellIdentifier")
        }
    }
    
    /// Метод для обновления списка валют
    private func updateVC(collectionView: UICollectionView) {
        DispatchQueue.main.async {
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.reloadData()
        }
    }
    
    private func updateTitle(index: Int) {
        guard let currency = dataSource?.getListCurrency(.top)[index] else { return }
        
        DispatchQueue.main.async { [weak self] in
            self?.title = String(currency.rate)
        }
    }
    
    @IBAction func actionExchange(_ sender: Any) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.dataSource?.exchange()
        }
    }
    
}

extension CurrencyExchangeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let dataSource = self.dataSource else { return 0 }
        guard let tag = CollectionTag(rawValue: collectionView.tag) else { return 0 }
        
        return dataSource.getListCurrency(tag).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCellIdentifier", for: indexPath) as! CurrencyExchangeCell
        
        guard let dataSource = self.dataSource else { return cell }
        guard let tag = CollectionTag(rawValue: collectionView.tag) else { return cell }
        
        let currency = dataSource.getListCurrency(tag)[indexPath.row]
        
        cell.configuration(name: currency.name, amount: currency.amount, purse: currency.purse, rate: String(currency.rate), delegate: self, tag: tag)
        
        return cell
    }
}

extension CurrencyExchangeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            guard let dataSource = self.dataSource else { return }
            guard let tag = CollectionTag(rawValue: collectionView.tag) else { return }
            self.updateTitle(index: indexPath.row)
            dataSource.setItem(index: indexPath.row, tag: tag)
        }
    }
}

extension CurrencyExchangeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

extension CurrencyExchangeViewController: CurrencyExchangeViewControllerDelegate {
    func responseCurrences() {
        self.updateVC(collectionView: self.fromCollectionView)
        self.updateVC(collectionView: self.toCollectionView)
    }
    
    func showAlert(title: String, message: String?, handler: ((UIAlertAction)->())? = nil) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okey = UIAlertAction(title: "Okay", style: .default, handler: handler)
            
            alert.addAction(okey)
            self.present(alert, animated: true)
        }
    }
}

extension CurrencyExchangeViewController: CurrencyExchangeCellDelegate {
    func setAmount(value: String, tag: CollectionTag) {
        DispatchQueue.main.async {
            guard let result = self.dataSource?.getCalculate(value, tag: tag) else { return }
            guard let fromCell = self.fromCollectionView.cellForItem(at: IndexPath(item: result.fromIndex, section: 0)) as? CurrencyExchangeCell else { return }
            guard let toCell = self.toCollectionView.cellForItem(at: IndexPath(item: result.toIndex, section: 0)) as? CurrencyExchangeCell else { return }
            
            fromCell.set(amount: result.fromAmount)
            toCell.set(amount: result.toAmount)
        }
    }
}
