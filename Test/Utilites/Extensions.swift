//
//  Extensions.swift
//  Test
//
//  Created by Макарский Р.Д. on 09.11.2019.
//  Copyright © 2019 Destplay. All rights reserved.
//

import Foundation

extension Double {
    
    func roundAndLimitted() -> Double {
        
        return ((abs(self)) * 100).rounded() / 100
    }
    
}
