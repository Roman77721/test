//
//  Log.swift
//  MobileAppForTrips
//
//  Created by Макарский Р.Д. on 14/11/2018.
//  Copyright © 2018 Vostochny. All rights reserved.
//

import Foundation

final class Log {
    
    static var info = true
    static var test = true
    static var error = true
    static var allOff = false
    static var warning = true
    
    static func i(_ content: Any? = "", tag: String? = "DEFAULT") {
        if (Log.info && !Log.allOff) {
            print("ℹ️(Information)", "[", tag!, "]", content ?? "", "\n")
        }
    }
    
    static func t(_ content: Any? = "", tag: String? = "DEFAULT") {
        if (Log.test && !Log.allOff) {
            print("🛠(Testing)", "[",tag!, "]",content ?? "", "\n")
        }
    }
    
    static func e(_ content: Any? = "", tag: String? = "DEFAULT") {
        if (Log.error && !Log.allOff) {
            print("⛔️(Error)", "[", tag!, "]", content ?? "", "\n")
        }
    }

    static func w(_ content: Any? = "", tag: String? = "DEFAULT") {
        if (Log.info && !Log.allOff) {
            print("⚠️(Warning)", "[", tag!, "]", content ?? "", "\n")
        }
    }
    
}
