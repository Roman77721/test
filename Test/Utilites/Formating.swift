//
//  Formating.swift
//  MobileAppForTrips
//
//  Created by Макарский Р.Д. on 19/11/2018.
//  Copyright © 2018 Vostochny. All rights reserved.
//

import Foundation
import CommonCrypto

enum TypeMask: String {
    case positive = "+*******"
    case negative = "-*******"
}

class Formating {
    
    func formatMask(text: String, typeMask: TypeMask) -> String {
        let cleanPhoneNumber = text.components(separatedBy: CharacterSet(charactersIn: ".0123456789").inverted).joined()
        
        let mask: String = typeMask.rawValue
        
        var result = String()
        var index = cleanPhoneNumber.startIndex
        for char in mask {
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if char == "*" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(char)
            }
        }
        return result
    }
    
    func getSymbol(forCurrencyCode code: String) -> String {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code) ?? ""
        }
        return locale.displayName(forKey: .currencySymbol, value: code) ?? ""
    }
}
